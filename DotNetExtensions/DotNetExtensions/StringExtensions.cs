﻿using DotNetExtensions.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions
{
    public static class StringExtensions
    {
        #region methods
        #region pattern matching
        #region Z algorithm
        public static ZBox[] GetZBoxes(this string source)
        {
            int sourceLength = source.Length;
            ZBox[] zBoxes = new ZBox[sourceLength];
            char[] sourceChars = source.ToCharArray();
            int tempIndex = 0;

            zBoxes[0] = new ZBox();

            for (int i = 1; i < sourceLength; )
            {
                if (i > zBoxes[i - 1].EndIndex)
                {
                    // if we are here it means that we are out of the previous position's ZBox.

                    tempIndex = 0;

                    if (sourceChars[tempIndex] == sourceChars[i + tempIndex])
                    {
                        zBoxes[i] = new ZBox(startIndex: i, size: 1);
                        i++;

                        while (sourceChars[tempIndex] == sourceChars[i + tempIndex])
                        {
                            zBoxes[i]++;
                            tempIndex++;
                        }
                    }
                    else
                    {
                        zBoxes[i] = new ZBox();
                    }
                }
                else
                {
                    // if we are here it means that we are inside the limits of the previous position's ZBox.

                    tempIndex = i - zBoxes[i - 1].StartIndex;

                    if (tempIndex > 0)
                    {
                        // if we are here it means that we are in a z-box placed
                        // significantly deep in the source string

                        // check if the z-box in the temp position has a size > 0
                        // check if the z-box in the temp position has a size greater than the remaining elements to analyze in the current z-box
                        // move current cursor accordingly
                        // move current cursor accordingly
                        // check for match between current char and char in temp position
                    }
                }
            }

            return zBoxes;
        }
        #endregion

        #region Boyer-Moore algorithm

        #endregion

        #region Knuth-Morris-Pratt algorithm

        #endregion
        #endregion
        #endregion
    }
}
