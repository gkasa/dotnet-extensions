﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions
{
    public static class GenericExtensions
    {
        #region methods
        /// <summary>
        /// Sum two numeric elements.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the elements.
        /// </typeparam>
        /// <param name="left">
        /// The left side of the operation.
        /// </param>
        /// <param name="right">
        /// The right side of the operation.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// In case T is not numeric.
        /// </exception>
        public static T Sum<T>(this T left, T right)
        {
            if (!typeof(T).IsNumericType())
            {
                throw new InvalidOperationException("The type of the operands must be numeric.");
            }

            T result = default(T);

            if (typeof(T) == typeof(byte))
            {
                result = (left.CastTo<byte>() + right.CastTo<byte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(decimal))
            {
                result = (left.CastTo<decimal>() + right.CastTo<decimal>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(double))
            {
                result = (left.CastTo<double>() + right.CastTo<double>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(float))
            {
                result = (left.CastTo<float>() + right.CastTo<float>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(int))
            {
                result = (left.CastTo<int>() + right.CastTo<int>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(long))
            {
                result = (left.CastTo<long>() + right.CastTo<long>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(sbyte))
            {
                result = (left.CastTo<sbyte>() + right.CastTo<sbyte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(short))
            {
                result = (left.CastTo<short>() + right.CastTo<short>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(uint))
            {
                result = (left.CastTo<uint>() + right.CastTo<uint>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ulong))
            {
                result = (left.CastTo<ulong>() + right.CastTo<ulong>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ushort))
            {
                result = (left.CastTo<ushort>() + right.CastTo<ushort>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(Complex))
            {
                result = (left.CastTo<Complex>() + right.CastTo<Complex>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(BigInteger))
            {
                result = (left.CastTo<BigInteger>() + right.CastTo<BigInteger>()).CastTo<T>();
            }

            return result;
        }

        /// <summary>
        /// Subtract two numeric elements.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the elements.
        /// </typeparam>
        /// <param name="left">
        /// The left side of the operation.
        /// </param>
        /// <param name="right">
        /// The right side of the operation.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// In case T is not numeric.
        /// </exception>
        public static T Subtract<T>(this T left, T right)
        {
            if (!typeof(T).IsNumericType())
            {
                throw new InvalidOperationException("The type of the operands must be numeric.");
            }

            T result = default(T);

            if (typeof(T) == typeof(byte))
            {
                result = (left.CastTo<byte>() - right.CastTo<byte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(decimal))
            {
                result = (left.CastTo<decimal>() - right.CastTo<decimal>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(double))
            {
                result = (left.CastTo<double>() - right.CastTo<double>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(float))
            {
                result = (left.CastTo<float>() - right.CastTo<float>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(int))
            {
                result = (left.CastTo<int>() - right.CastTo<int>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(long))
            {
                result = (left.CastTo<long>() - right.CastTo<long>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(sbyte))
            {
                result = (left.CastTo<sbyte>() - right.CastTo<sbyte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(short))
            {
                result = (left.CastTo<short>() - right.CastTo<short>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(uint))
            {
                result = (left.CastTo<uint>() - right.CastTo<uint>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ulong))
            {
                result = (left.CastTo<ulong>() - right.CastTo<ulong>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ushort))
            {
                result = (left.CastTo<ushort>() - right.CastTo<ushort>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(Complex))
            {
                result = (left.CastTo<Complex>() - right.CastTo<Complex>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(BigInteger))
            {
                result = (left.CastTo<BigInteger>() - right.CastTo<BigInteger>()).CastTo<T>();
            }

            return result;
        }

        /// <summary>
        /// Multiply two numeric elements.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the elements.
        /// </typeparam>
        /// <param name="left">
        /// The left side of the operation.
        /// </param>
        /// <param name="right">
        /// The right side of the operation.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// In case T is not numeric.
        /// </exception>
        public static T Multiply<T>(this T left, T right)
        {
            if (!typeof(T).IsNumericType())
            {
                throw new InvalidOperationException("The type of the operands must be numeric.");
            }

            T result = default(T);

            if (typeof(T) == typeof(byte))
            {
                result = (left.CastTo<byte>() * right.CastTo<byte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(decimal))
            {
                result = (left.CastTo<decimal>() * right.CastTo<decimal>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(double))
            {
                result = (left.CastTo<double>() * right.CastTo<double>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(float))
            {
                result = (left.CastTo<float>() * right.CastTo<float>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(int))
            {
                result = (left.CastTo<int>() * right.CastTo<int>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(long))
            {
                result = (left.CastTo<long>() * right.CastTo<long>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(sbyte))
            {
                result = (left.CastTo<sbyte>() * right.CastTo<sbyte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(short))
            {
                result = (left.CastTo<short>() * right.CastTo<short>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(uint))
            {
                result = (left.CastTo<uint>() * right.CastTo<uint>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ulong))
            {
                result = (left.CastTo<ulong>() * right.CastTo<ulong>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ushort))
            {
                result = (left.CastTo<ushort>() * right.CastTo<ushort>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(Complex))
            {
                result = (left.CastTo<Complex>() * right.CastTo<Complex>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(BigInteger))
            {
                result = (left.CastTo<BigInteger>() * right.CastTo<BigInteger>()).CastTo<T>();
            }

            return result;
        }

        /// <summary>
        /// Divide two numeric elements.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the elements.
        /// </typeparam>
        /// <param name="left">
        /// The left side of the operation.
        /// </param>
        /// <param name="right">
        /// The right side of the operation.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        /// <exception cref="InvalidOperationException">
        /// In case T is not numeric.
        /// </exception>
        public static T Divide<T>(this T left, T right)
        {
            if (!typeof(T).IsNumericType())
            {
                throw new InvalidOperationException("The type of the operands must be numeric.");
            }

            T result = default(T);

            if (typeof(T) == typeof(byte))
            {
                result = (left.CastTo<byte>() / right.CastTo<byte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(decimal))
            {
                result = (left.CastTo<decimal>() / right.CastTo<decimal>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(double))
            {
                result = (left.CastTo<double>() / right.CastTo<double>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(float))
            {
                result = (left.CastTo<float>() / right.CastTo<float>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(int))
            {
                result = (left.CastTo<int>() / right.CastTo<int>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(long))
            {
                result = (left.CastTo<long>() / right.CastTo<long>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(sbyte))
            {
                result = (left.CastTo<sbyte>() / right.CastTo<sbyte>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(short))
            {
                result = (left.CastTo<short>() / right.CastTo<short>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(uint))
            {
                result = (left.CastTo<uint>() / right.CastTo<uint>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ulong))
            {
                result = (left.CastTo<ulong>() / right.CastTo<ulong>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(ushort))
            {
                result = (left.CastTo<ushort>() / right.CastTo<ushort>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(Complex))
            {
                result = (left.CastTo<Complex>() / right.CastTo<Complex>()).CastTo<T>();
            }
            else if (typeof(T) == typeof(BigInteger))
            {
                result = (left.CastTo<BigInteger>() / right.CastTo<BigInteger>()).CastTo<T>();
            }

            return result;
        }
        #endregion
    }
}
