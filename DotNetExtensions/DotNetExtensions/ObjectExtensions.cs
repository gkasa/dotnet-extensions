﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions
{
    public static class ObjectExtensions
    {
        #region methods
        /// <summary>
        /// A method that checks if an object is a (standard) numeric or not.
        /// </summary>
        /// <param name="obj">
        /// The object to check for.
        /// </param>
        /// <returns>
        /// TRUE if the specified object is a (standard) numeric, FALSE otherwise.
        /// </returns>
        /// <remarks>
        /// The standard numeric types taken into consideration are:
        ///     - byte
        ///     - sbyte
        ///     - decimal
        ///     - double
        ///     - float
        ///     - int
        ///     - uint
        ///     - long
        ///     - ulong
        ///     - short
        ///     - ushort
        ///     - Complex
        ///     - BigInteger.
        /// </remarks>
        public static bool IsNumeric(this object obj)
        {
            return obj is byte
                || obj is decimal
                || obj is double
                || obj is float
                || obj is int
                || obj is long
                || obj is sbyte
                || obj is short
                || obj is uint
                || obj is ulong
                || obj is ushort
                || obj is Complex
                || obj is BigInteger;
        }

        /// <summary>
        /// Unsafely casts an object to a specified type.
        /// </summary>
        /// <typeparam name="DType">
        /// The destination type.
        /// </typeparam>
        /// <param name="obj">
        /// The object to cast.
        /// </param>
        /// <returns>
        /// The object cast to the destination type.
        /// </returns>
        public static DType CastTo<DType>(this object obj)
        {
            return (DType)obj;
        }
        #endregion
    }
}
