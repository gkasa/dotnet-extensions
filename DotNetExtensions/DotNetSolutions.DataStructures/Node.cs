﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSolutions.DataStructures
{
    public abstract class Node<T>
    {
        #region variables

        #endregion

        #region properties
        /// <summary>
        /// Get or Set the item of this node.
        /// </summary>
        public T Item { get; set; }

        /// <summary>
        /// Get or Set the children of this node.
        /// </summary>
        public ICollection<Node<T>> Children { get; set; }
        
        /// <summary>
        /// Get or Set the parent of this node.
        /// </summary>
        public Node<T> Parent { get; set; }
        #endregion

        #region methods
        /// <summary>
        /// Insert a new child in the subtree of this node.
        /// </summary>
        /// <param name="element">
        /// The element to insert in the subtree of this node.
        /// </param>
        public abstract void Insert(T element);

        /// <summary>
        /// Search for a desired element in the subtree starting from this node.
        /// </summary>
        /// <param name="element">
        /// The element to search for.
        /// </param>
        /// <returns>
        /// The node containing the desired element.
        /// </returns>
        public abstract Node<T> Search(T element);

        /// <summary>
        /// Remove a given element from the subtree of this node.
        /// </summary>
        /// <param name="element">
        /// The element to remove from the subtree.
        /// </param>
        /// <returns>
        /// The (removed) node containing the removed element.
        /// </returns>
        public abstract Node<T> Remove(T element);
        #endregion
    }
}
