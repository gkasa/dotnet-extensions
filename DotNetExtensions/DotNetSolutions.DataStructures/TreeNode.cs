﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSolutions.DataStructures
{
    public class TreeNode<T> : Node<T>
    {
        #region properties

        #endregion

        #region constructors
        /// <summary>
        /// The default constructor of a TreeNode instance.
        /// </summary>
        /// <remarks>
        /// The item of the node is the default for the type of the elements.
        /// No parent and no children nodes are assigned.
        /// </remarks>
        public TreeNode() : this(default(T)) { }

        /// <summary>
        /// A constructor of a TreeNode instance.
        /// </summary>
        /// <param name="item">
        /// The item this node will contain.
        /// </param>
        public TreeNode(T item) : this(item, null) { }

        /// <summary>
        /// A constructor of a TreeNode instance.
        /// </summary>
        /// <param name="item">
        /// The item this node will contain.
        /// </param>
        /// <param name="parent">
        /// The parent node of this TreeNode instance.
        /// </param>
        public TreeNode(T item, TreeNode<T> parent) : this(item, parent, new List<TreeNode<T>>()) { }

        /// <summary>
        /// A constructor of a TreeNode instance.
        /// </summary>
        /// <param name="item">
        /// The item this node will contain.
        /// </param>
        /// <param name="parent">
        /// The parent node of this TreeNode instance.
        /// </param>
        /// <param name="children">
        /// The children of this TreeNode instance.
        /// </param>
        public TreeNode(T item, TreeNode<T> parent, ICollection<TreeNode<T>> children)
        {
            Item = item;
            Parent = parent;
            Children = FromTreeNodeCollection(children);
        }
        #endregion

        #region methods
        /// <summary>
        /// Insert a new element as a child of this TreeNode.
        /// </summary>
        /// <param name="element">
        /// The element to be inserted as a child of this TreeNode.
        /// </param>
        public override void Insert(T element)
        {
            Children.Add(new TreeNode<T>(element, this));
        }

        /// <summary>
        /// Search for the given element in the subtree of this TreeNode.
        /// </summary>
        /// <param name="element">
        /// The element to search for in the subtree of this TreeNode.
        /// </param>
        /// <returns>
        /// The Node containing the desired element (if found), NULL otherwise.
        /// </returns>
        public override Node<T> Search(T element)
        {
            return BreadthFirstSearch(element);
        }

        public TreeNode<T> BreadthFirstSearch(T element)
        {
            return BreadthFirstSearch(element, optimizeSpace: false);
        }

        public TreeNode<T> BreadthFirstSearch(T element, bool optimizeSpace)
        {
            TreeNode<T> result = null;
            List<TreeNode<T>> visitedNodes = (optimizeSpace ? new List<TreeNode<T>>() : null);
            var nodesToVisit = new List<TreeNode<T>>();
            TreeNode<T> currentNode = null;
            bool found = false;

            nodesToVisit.Add(this);

            do
            {
                currentNode = nodesToVisit.First();
                nodesToVisit.RemoveAt(0);
                found = currentNode.Item.Equals(element);

                if (found)
                {
                    result = currentNode;
                }
                else
                {
                    if (!optimizeSpace)
                    {
                        visitedNodes.Add(currentNode);
                    }

                    nodesToVisit.AddRange(FromNodeCollection(currentNode.Children));
                }
            } while (nodesToVisit.Count > 0 && !found);

            return result;
        }

        public TreeNode<T> DepthFirstSearch(T element)
        {
            TreeNode<T> result = null;
            var nodesToVisit = new Stack<TreeNode<T>>();
            TreeNode<T> currentNode = null;
            bool found = false;
            int childrenCount = 0;

            nodesToVisit.Push(this);

            do
            {
                currentNode = nodesToVisit.Pop();
                found = currentNode.Item.Equals(element);

                if (found)
                {
                    result = currentNode;
                }
                else
                {
                    childrenCount = currentNode.Children.Count;

                    for (int i = 0; i < childrenCount; i++)
                    {
                        nodesToVisit.Push((TreeNode<T>)(currentNode.Children.ElementAt(i)));
                    }
                }
            } while (!found && nodesToVisit.Count > 0);

            return result;
        }

        public TreeNode<T> LimitedDepthFirstSearch(T element, int maxDepth)
        {
            if (maxDepth < 0)
            {
                return DepthFirstSearch(element);
            }

            TreeNode<T> result = null;
            var nodesToVisit = new Stack<KeyValuePair<TreeNode<T>, int>>();
            TreeNode<T> currentNode = null;
            KeyValuePair<TreeNode<T>, int> currentElement;
            int currentDepth = 0;
            bool found = false;
            int currentChildrenCount = 0;

            nodesToVisit.Push(new KeyValuePair<TreeNode<T>, int>(this, currentDepth));

            do
            {
                currentElement = nodesToVisit.Pop();
                currentNode = currentElement.Key;
                currentDepth = currentElement.Value;
                found = currentNode.Item.Equals(element);

                if (found)
                {
                    result = currentNode;
                }
                else if(currentDepth < maxDepth)
                {
                    currentChildrenCount = currentNode.Children.Count;

                    for (int i = 0; i < currentChildrenCount; i++)
                    {
                        nodesToVisit.Push(new KeyValuePair<TreeNode<T>, int>(currentNode, currentDepth + 1));
                    }
                }
            } while (!found && nodesToVisit.Count > 0);

            return result;
        }

        public TreeNode<T> IterativeDeepening(T element, int maxDepth)
        {
            if (maxDepth < 1)
            {
                throw new ArgumentException("The value cannot be smaller than or equal to 0.", "maxDepth");
            }

            TreeNode<T> result = null;

            for (int i = 0; i < maxDepth && result == null; i++)
            {
                result = LimitedDepthFirstSearch(element, i);
            }

            return result;
        }

        public override Node<T> Remove(T element)
        {
            throw new NotImplementedException();
        }

        private static ICollection<TreeNode<T>> FromNodeCollection(ICollection<Node<T>> source)
        {
            int count = source.Count;

            if (count > 0 && !(source.First() is TreeNode<T>))
            {
                throw new ArgumentException("The type of nodes of the source collection is not TreeNode<T>.");
            }

            var result = new List<TreeNode<T>>();

            for (int i = 0; i < count; i++)
            {
                result.Add(source.ElementAt(i) as TreeNode<T>);
            }

            return result;
        }

        private static ICollection<Node<T>> FromTreeNodeCollection(ICollection<TreeNode<T>> source)
        {
            int count = source.Count;
            var result = new List<Node<T>>();

            for (int i = 0; i < count; i++)
            {
                result.Add(source.ElementAt(i));
            }

            return result;
        }
        #endregion
    }
}
