﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetSolutions.DataStructures
{
    public interface ITree<T>
    {
        /// <summary>
        /// Insert a new element in the tree.
        /// </summary>
        /// <param name="element">
        /// The element to insert in the tree.
        /// </param>
        void Insert(T element);

        /// <summary>
        /// Search for a given element in the tree.
        /// </summary>
        /// <param name="element">
        /// The element to search for.
        /// </param>
        /// <returns>
        /// The found element, if it was found.
        /// </returns>
        T Search(T element);

        /// <summary>
        /// Remove a given element from the tree.
        /// </summary>
        /// <param name="element">
        /// The element to be removed from the tree.
        /// </param>
        /// <returns>
        /// The removed element.
        /// </returns>
        T Remove(T element);
    }
}
