﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    /// <summary>
    /// An abstract class that contains definition and some methods for generic Matrices.
    /// </summary>
    /// <remarks>
    /// By default this class manages the .NET framework numerik types:
    ///     - byte
    ///     - decimal
    ///     - double
    ///     - float
    ///     - int
    ///     - long
    ///     - sbyte
    ///     - short
    ///     - uint
    ///     - ulong
    ///     - ushort
    ///     - Complex
    ///     - BigInteger
    /// However, given a custom type with the defined following components:
    ///     - Sum function
    ///     - Multiplication function
    ///     - Subtraction function
    ///     - Sum identity
    ///     - Multiplication identity
    /// it is possible to manage that custom type also.
    /// 
    /// Next development steps:
    ///     - Improve Exception management
    ///         - Add error-safe methods (TryAdd, TryMultiply, etc.)
    ///     - Add logging instructions
    /// </remarks>
    public abstract class Matrix<T>
    {
        #region variables
        /// <summary>
        /// The inner structure for the elements of the Matrix
        /// </summary>
        //private T[,] elements;

        //private object thisLock = new object();

        private Func<T, T, T> sumFunction = Utilities.Sum<T>();
        private Func<T, T, T> multiplicationFunction = Utilities.Multiplication<T>();
        private Func<T, T, T> divisionFunction = Utilities.Division<T>();
        private Func<T, T, T> subtractionFunction = Utilities.Subtraction<T>();
        private T sumIdentity = Utilities.SumId<T>();
        private T multiplicationIdentity = Utilities.MultiplicationId<T>();
        #endregion

        #region properties
        /// <summary>
        /// Get the number of the columns in this matrix.
        /// </summary>
        public abstract int Columns { get; }

        /// <summary>
        /// Get the number of rows in this matrix.
        /// </summary>
        public abstract int Rows { get; }

        /// <summary>
        /// The sum function for the items of the matrix.
        /// </summary>
        public virtual Func<T, T, T> ItemSumFunction
        {
            get
            {
                return sumFunction;
            }
            set
            {
                sumFunction = value;
            }
        }

        /// <summary>
        /// The multiplication function for the items of the matrix.
        /// </summary>
        public virtual Func<T, T, T> ItemMultiplicationFunction
        {
            get
            {
                return multiplicationFunction;
            }
            set
            {
                multiplicationFunction = value;
            }
        }

        /// <summary>
        /// The subtraction function for the items of the matrix.
        /// </summary>
        public virtual Func<T, T, T> ItemSubtractionFunction
        {
            get
            {
                return subtractionFunction;
            }
            set
            {
                subtractionFunction = value;
            }
        }

        /// <summary>
        /// The division function for the items of the matrix.
        /// </summary>
        public virtual Func<T, T, T> ItemDivisionFunction
        {
            get
            {
                return divisionFunction;
            }
            set
            {
                divisionFunction = value;
            }
        }

        public virtual T ItemSumIdentity
        {
            get
            {
                return sumIdentity;
            }
            set
            {
                sumIdentity = value;
            }
        }

        public virtual T ItemMultiplicationIdentity
        {
            get
            {
                return multiplicationIdentity;
            }
            set
            {
                multiplicationIdentity = value;
            }
        }

        /// <summary>
        /// Check if this Matrix is a square one or not.
        /// </summary>
        public virtual bool IsSquare
        {
            get
            {
                return Rows == Columns;
            }
        }

        ///// <summary>
        ///// Check asynchronously if this Matrix is a square one or not.
        ///// </summary>
        //public Task<bool> IsSquareAsync
        //{
        //    get
        //    {
        //        return Task.Run(() => IsSquare);
        //    }
        //}

        /// <summary>
        /// Check if this Matrix is a symmetric one or not.
        /// </summary>
        public virtual bool IsSymmetric
        {
            get
            {
                bool result = IsSquare;

                //lock (thisLock)
                //{
                for (int i = 0; result && i < Rows; i++)
                {
                    for (int j = i; result && j < Columns; j++)
                    {
                        //result = result && (elements[i, j].Equals(elements[j, i]));
                        result = result && this[i, j].Equals(this[j, i]);
                    }
                }
                //}

                return result;
            }
        }

        /// <summary>
        /// Get the elements contained in this matrix.
        /// </summary>
        public virtual T[,] Elements
        {
            get
            {
                T[,] result = new T[Rows, Columns];

                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        result[i, j] = this[i, j];
                    }
                }
                
                return result;
            }
        }

        /// <summary>
        /// Get or Set the value of the element in the given position.
        /// </summary>
        /// <param name="row">
        /// The 0-based index of the row.
        /// </param>
        /// <param name="column">
        /// The 0-based index of the column.
        /// </param>
        /// <returns>
        /// The element in the specified position.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Exception thrown in case the specified index is out of the allowed range.
        /// </exception>
        public abstract T this[int row, int column]
        {
            get;
            //{
            //    if (row < 0)
            //    {
            //        throw new IndexOutOfRangeException(
            //            "The index of the row cannot be smaller than 0. The row index is 0-based.");
            //    }

            //    if (row >= Rows)
            //    {
            //        throw new IndexOutOfRangeException(
            //            string.Format(
            //                "The index of the row cannot be greater than {0}.",
            //                (Rows - 1)));
            //    }

            //    if (column < 0)
            //    {
            //        throw new IndexOutOfRangeException(
            //            "The index of the column cannot be smaller than 0. The row index is 0-based.");
            //    }

            //    if (column >= Columns)
            //    {
            //        throw new IndexOutOfRangeException(
            //            string.Format(
            //                "The index of the column cannot be greater than {0}.",
            //                (Columns - 1)));
            //    }

            //    T result;

            //    //lock (elements)
            //    //{
            //    result = elements[row, column];
            //    //}

            //    return result;
            //}
            set;
            //{
            //    if (row < 0)
            //    {
            //        throw new IndexOutOfRangeException(
            //            "The index of the row cannot be smaller than 0. The row index is 0-based.");
            //    }

            //    if (row >= Rows)
            //    {
            //        throw new IndexOutOfRangeException(
            //            string.Format(
            //                "The index of the row cannot be greater than {0}.",
            //                (Rows - 1)));
            //    }

            //    if (column < 0)
            //    {
            //        throw new IndexOutOfRangeException(
            //            "The index of the column cannot be smaller than 0. The row index is 0-based.");
            //    }

            //    if (column >= Columns)
            //    {
            //        throw new IndexOutOfRangeException(
            //            string.Format(
            //                "The index of the column cannot be greater than {0}.",
            //                (Columns - 1)));
            //    }

            //    elements[row, column] = value;
            //}
        }
        #endregion

        //#region constructors
        ///// <summary>
        ///// The default constructor of a Matrix.
        ///// </summary>
        ///// <remarks>
        ///// This constructor builds 2x2 Matrix.
        ///// </remarks>
        //public Matrix() : this(dimension: 2) { }

        ///// <summary>
        ///// A constructor of a square Matrix instance.
        ///// </summary>
        ///// <param name="dimension">
        ///// The dimension of the square Matrix.
        ///// </param>
        //public Matrix(int dimension) : this(dimension, dimension) { }

        ///// <summary>
        ///// A constructor of a generic Matrix instance.
        ///// </summary>
        ///// <param name="rows">
        ///// The number of rows the new instance must have.
        ///// </param>
        ///// <param name="columns">
        ///// The number of columns the new instance must have.
        ///// </param>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified number of rows and/or columns is smaller than 1.
        ///// </exception>
        //public Matrix(int rows, int columns)
        //{
        //    if (rows < 0)
        //    {
        //        throw new ArgumentOutOfRangeException("rows", "The number of rows must be at least 0.");
        //    }

        //    if (columns < 0)
        //    {
        //        throw new ArgumentOutOfRangeException("columns", "The number of columns must be at least 0.");
        //    }


        //    Rows = rows;
        //    Columns = columns;
        //    elements = new T[Rows, Columns];
        //}
        //#endregion

        #region methods
        public Matrix<T> CreateNewInstance()
        {
            return CreateNewInstance(2);
        }

        public Matrix<T> CreateNewInstance(int dimension)
        {
            return CreateNewInstance(dimension, dimension);
        }

        public abstract Matrix<T> CreateNewInstance(int rows, int columns);

        /// <summary>
        /// Sum a matrix with this one.
        /// </summary>
        /// <param name="toAdd">
        /// The matrix to sum with this one.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Sum(Matrix<T> toAdd)
        {
            if (Rows != toAdd.Rows && Columns != toAdd.Columns)
            {
                throw new InvalidOperationException("The rows and the columns of the matrices must be the same.");
            }

            var result = CreateNewInstance(Rows, Columns); //new StaticMatrix<T>(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemSumFunction(this[i, j], toAdd[i, j]);
                }
            }

            return result;
        }

        /// <summary>
        /// Sum this matrix with an item instance.
        /// </summary>
        /// <param name="toAdd">
        /// The item instance to sum with this matrix.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Sum(T toAdd)
        {
            var result = CreateNewInstance(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemSumFunction(this[i, j], toAdd);
                }
            }

            return result;
        }

        /// <summary>
        /// Subtract a matrix from this one.
        /// </summary>
        /// <param name="toAdd">
        /// The matrix to subtract to this one.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Subtract(Matrix<T> toSubtract)
        {
            if (Rows != toSubtract.Rows && Columns != toSubtract.Columns)
            {
                throw new InvalidOperationException("The rows and the columns of the matrices must be the same.");
            }

            var result = CreateNewInstance(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemSubtractionFunction(this[i, j], toSubtract[i, j]);
                }
            }

            return result;            
        }

        /// <summary>
        /// Subtract an item instance from this matrix.
        /// </summary>
        /// <param name="toAdd">
        /// The item instance to subtract to this matrix.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Subtract(T toSubtract)
        {
            var result = CreateNewInstance(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemSubtractionFunction(this[i, j], toSubtract);
                }
            }

            return result;
        }

        /// <summary>
        /// Multiply this Matrix with another one.
        /// </summary>
        /// <param name="multiplicator">
        /// The Matrix that mulplies this Matrix.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Multiply(Matrix<T> multiplicator)
        {
            if (Columns != multiplicator.Rows)
            {
                throw new InvalidOperationException("This operation cannot be completed due to wrong matrix dimensions.");
            }

            var result = CreateNewInstance(Rows, multiplicator.Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    for (int k = 0; k < multiplicator.Columns; k++)
                    {
                        result[i, k] = this.ItemSumFunction(
                            result[i, k],
                            this.ItemMultiplicationFunction(this[i, j], multiplicator[j, k]));
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Multiply this Matrix with a scalar.
        /// </summary>
        /// <param name="multiplicator">
        /// The scalar that multiplies this Matrix.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Multiply(T multiplicator)
        {
            var result = CreateNewInstance(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemMultiplicationFunction(this[i, j], multiplicator);
                }
            }

            return result;
        }

        /// <summary>
        /// Divide this Matrix with a scalar.
        /// </summary>
        /// <param name="dividend">
        /// The scalar that divides the elements of this Matrix.
        /// </param>
        /// <returns>
        /// The result of the operation.
        /// </returns>
        public virtual Matrix<T> Divide(T dividend)
        {
            var result = CreateNewInstance(Rows, Columns);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[i, j] = this.ItemDivisionFunction(this[i, j], dividend);
                }
            }

            return result;
        }

        /// <summary>
        /// A method that builds the transposed Matrix of this Matrix.
        /// </summary>
        /// <returns>
        /// The transposed Matrix of this Matrix instance.
        /// </returns>
        public virtual Matrix<T> Transpose()
        {
            Matrix<T> result = CreateNewInstance(rows: Columns, columns: Rows);

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result[j, i] = this[i, j];
                }
            }

            return result;
        }

        /// <summary>
        /// Get the row with the specified index.
        /// </summary>
        /// <param name="index">
        /// The index of the desired row.
        /// </param>
        /// <returns>
        /// The row with the specified index.
        /// </returns>
        /// <remarks>
        /// The index is 0-based.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if the specified index is not in [0, Rows).
        /// </exception>
        public virtual T[] GetRow(int index)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "The indicated index cannot be smaller than 0.");
            }

            if (index >= Rows)
            {
                throw new ArgumentOutOfRangeException("index", string.Format("The indicated index cannot be greater than {0}.", Rows - 1));
            }

            T[] result = new T[Columns];

            for (int i = 0; i < Columns; i++)
            {
                result[i] = this[index, i];
            }

            return result;
        }

        /// <summary>
        /// Get the column with the specified index.
        /// </summary>
        /// <param name="index">
        /// The index of the desired column.
        /// </param>
        /// <returns>
        /// The column with the specified index.
        /// </returns>
        /// <remarks>
        /// The index is 0-based.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if the specified index is not in [0, Columns).
        /// </exception>
        public virtual T[] GetColumn(int index)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "The indicated index cannot be smaller than 0.");
            }

            if (index >= Columns)
            {
                throw new ArgumentOutOfRangeException("index", string.Format("The indicated index cannot be greater than {0}.", Rows - 1));
            }

            T[] result = new T[Rows];

            for (int i = 0; i < Rows; i++)
            {
                result[i] = this[i, index];
            }

            return result;
        }

        /// <summary>
        /// A method that checks if a given object is equal to 
        /// this Matrix.
        /// </summary>
        /// <param name="obj">
        /// The object to check for equality with this Matrix.
        /// </param>
        /// <returns>
        /// TRUE if obj is equal to this Matrix, FALSE otherwise.
        /// </returns>
        public override bool Equals(object obj)
        {
            return (obj is Matrix<T>) && this.Equals((Matrix<T>)obj);
        }

        /// <summary>
        /// A method that checks if this Matrix is equal to another Matrix.
        /// </summary>
        /// <param name="other">
        /// The other Matrix to check for equality.
        /// </param>
        /// <returns>
        /// TRUE if the two matrices are equal, FALSE otherwise.
        /// </returns>
        public abstract bool Equals(Matrix<T> other);
        
        /// <summary>
        /// Get the string representation of the Matrix.
        /// </summary>
        /// <returns>
        /// The string representation of the Matrix.
        /// </returns>
        public override string ToString()
        {
            string result = "";

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    result += string.Format("{0}\t", this[i, j]);
                }

                result += "\n";
            }

            return result;
        }

        /// <summary>
        /// Get the hash code for this Matrix.
        /// </summary>
        /// <returns>
        /// The hash code for this Matrix instance.
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = 0;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    hashCode ^= this[i, j].GetHashCode();
                }
            }

            return hashCode;
        }

        /// <summary>
        /// Copy this matrix to a destination Matrix of the 
        /// specified size.
        /// </summary>
        /// <param name="dimension">
        /// The dimension of the square matrix resulting 
        /// from the resizing.
        /// </param>
        /// <returns>
        /// The square matrix resulting from the resizing.
        /// </returns>
        public virtual Matrix<T> Copy(int dimension = 2)
        {
            return Copy(dimension, dimension);
        }

        /// <summary>
        /// Copy this matrix to a destination Matrix of the 
        /// specified size.
        /// </summary>
        /// <param name="rows">
        /// The desired number of rows.
        /// </param>
        /// <param name="cols">
        /// The desired number of columns.
        /// </param>
        /// <returns>
        /// The square matrix resulting from the resizing.
        /// </returns>
        public virtual Matrix<T> Copy(int rows, int cols)
        {
            Matrix<T> result = CreateNewInstance(rows, cols);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    result[i, j] = i < Rows && j < Columns
                                    ? this[i, j]
                                    : default(T);
                }
            }

            return result;
        }

        /// <summary>
        /// Clone the structure and data of this Matrix.
        /// </summary>
        /// <returns>
        /// A clone of this Matrix.
        /// </returns>
        public virtual Matrix<T> Clone()
        {
            return Copy(Rows, Columns);
        }
        #endregion

        #region operators
        /// <summary>
        /// Get the Matrix result of the sum between two other matrices.
        /// </summary>
        /// <param name="left">
        /// The Matrix on the left side of the operator.
        /// </param>
        /// <param name="right">
        /// The Matrix on the right side of the operator.
        /// </param>
        /// <returns>
        /// The Matrix result of the sum between the two specified matrices.
        /// </returns>
        /// <remarks>
        /// This operator works correctly for the .NET's standard
        /// numeric types.
        /// </remarks>
        public static Matrix<T> operator +(Matrix<T> left, Matrix<T> right)
        {
            return left.Sum(right);
        }

        public static Matrix<T> operator +(Matrix<T> matrix, T scalar)
        {
            return matrix.Sum(scalar);
        }
        
        public static Matrix<T> operator -(Matrix<T> left, Matrix<T> right)
        {
            return left.Subtract(right);
        }

        public static Matrix<T> operator -(Matrix<T> matrix, T scalar)
        {
            return matrix.Subtract(scalar);
        }

        /// <summary>
        /// Get the Matrix result of the multiplication between 
        /// two other matrices.
        /// </summary>
        /// <param name="left">
        /// The Matrix on the left side of the operator.
        /// </param>
        /// <param name="right">
        /// The Matrix on the right side of the operator.
        /// </param>
        /// <returns>
        /// The Matrix result of the multiplication between the 
        /// two specified matrices.
        /// </returns>
        /// <remarks>
        /// This operator works correctly for the .NET's standard
        /// numeric types.
        /// </remarks>
        public static Matrix<T> operator *(Matrix<T> left, Matrix<T> right)
        {
            return left.Multiply(right);
        }

        /// <summary>
        /// Get the Matrix result of the multiplication between 
        /// a Matrix and a scalar value.
        /// </summary>
        /// <param name="matrix">
        /// The Matrix on the left side of the operator.
        /// </param>
        /// <param name="scalar">
        /// The scalar value on the right side of the operator.
        /// </param>
        /// <returns>
        /// The Matrix result of the multiplication between the 
        /// Matrix and the scalar.
        /// </returns>
        /// <remarks>
        /// This operator works correctly for the .NET's standard
        /// numeric types.
        /// </remarks>
        public static Matrix<T> operator *(Matrix<T> matrix, T scalar)
        {
            return matrix.Multiply(scalar);
        }

        /// <summary>
        /// Get the Matrix result of the multiplication between 
        /// a scalar value and a Matrix.
        /// </summary>
        /// <param name="left">
        /// The scalar value on the left side of the operator.
        /// </param>
        /// <param name="right">
        /// The Matrix on the right side of the operator.
        /// </param>
        /// <returns>
        /// The Matrix result of the multiplication between the 
        /// Matrix and the scalar.
        /// </returns>
        /// <remarks>
        /// This operator works correctly for the .NET's standard
        /// numeric types.
        /// </remarks>
        public static Matrix<T> operator *(T left, Matrix<T> right)
        {
            return right.Multiply(left);
        }

        /// <summary>
        /// Check if two matrices are equal.
        /// </summary>
        /// <param name="left">
        /// The scalar value on the left side of the operator.
        /// </param>
        /// <param name="right">
        /// The Matrix on the right side of the operator.
        /// </param>
        /// <returns>
        /// TRUE if the two matrices are equal, FALSE otherwise.
        /// </returns>
        public static bool operator ==(Matrix<T> left, Matrix<T> right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Check if two matrices are different.
        /// </summary>
        /// <param name="left">
        /// The scalar value on the left side of the operator.
        /// </param>
        /// <param name="right">
        /// The Matrix on the right side of the operator.
        /// </param>
        /// <returns>
        /// TRUE if the two matrices are different, FALSE otherwise.
        /// </returns>
        public static bool operator !=(Matrix<T> left, Matrix<T> right)
        {
            return !(left.Equals(right));
        }
        #endregion
    }
}
