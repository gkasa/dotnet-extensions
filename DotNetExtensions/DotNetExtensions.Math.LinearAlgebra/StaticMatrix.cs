﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    public class StaticMatrix<T> : Matrix<T>
    {
        #region variables
        // the number of columns
        private int columns;

        // the number of rows
        private int rows;

        // the items of the matrix
        private T[,] items;
        #endregion

        #region properties
        public override int Columns
        {
            get { return columns; }
        }

        public override int Rows
        {
            get { return rows; }
        }

        public override T this[int row, int column]
        {
            get { return items[row, column]; }
            set { items[row, column] = value; }
        }
        #endregion

        #region constructors
        /// <summary>
        /// Creates a square 2x2 matrix.
        /// </summary>
        public StaticMatrix() : this(2) { }

        /// <summary>
        /// Creates a square matrix with the given dimension.
        /// </summary>
        /// <param name="dimension">
        /// The dimension of the matrix.
        /// </param>
        public StaticMatrix(int dimension) : this(dimension, dimension) { }

        /// <summary>
        /// Creates a new StaticMatrix instance.
        /// </summary>
        /// <param name="rowNumber">
        /// The number of rows of the matrix.
        /// </param>
        /// <param name="colNumber">
        /// The number of columns of the matrix.
        /// </param>
        public StaticMatrix(int rowNumber, int colNumber)
        {
            rows = rowNumber;
            columns = colNumber;
            items = new T[Rows, Columns];
        }
        #endregion

        #region methods
        public override bool Equals(object obj)
        {
            return (obj is StaticMatrix<T>) && this.Equals(obj as StaticMatrix<T>);
        }

        public override bool Equals(Matrix<T> other)
        {
            //throw new NotImplementedException();
            return (other is StaticMatrix<T>) || this.Equals(other as StaticMatrix<T>);
        }

        public bool Equals(StaticMatrix<T> other)
        {
            var result = (Rows == other.Rows) && (Columns == other.Columns);

            for (int i = 0; result && i < Rows; i++)
            {
                for (int j = 0; result && j < Columns; j++)
                {
                    result = this[i, j].Equals(other[i, j]);
                }
            }

            return result;
        }

        public override Matrix<T> CreateNewInstance(int rows, int columns)
        {
            return new StaticMatrix<T>(rows, columns);
        }
        #endregion

    }
}
