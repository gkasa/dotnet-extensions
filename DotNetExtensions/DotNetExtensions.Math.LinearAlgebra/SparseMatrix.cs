﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    /// <summary>
    /// A class for the sparse matrices.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the elements of the single matrix instance.
    /// </typeparam>
    public abstract class SparseMatrix<T> : Matrix<T>
    {
        #region properties
        public abstract Dictionary<Tuple<int, int>, T> SparseMatrixElements { get; }
        #endregion

        #region methods
        public override Matrix<T> CreateNewInstance(int rows, int columns)
        {
            return CreateNewSparseMatrix<T>(rows, columns);
        }

        public abstract SparseMatrix<T> CreateNewSparseMatrix<T>(int rows, int columns);
        #endregion
    }
}
