﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    public class NumericMatrix<T> //: Matrix<T>
    {
        //#region variables
        ////private object thisLock = new object();
        //#endregion

        //#region properties
        ///// <summary>
        ///// Get or Set the multiplication function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> T</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T2) = T1 * T2</b>
        ///// </remarks>
        //public Func<T, T, T> Multiplication { get; set; }

        ///// <summary>
        ///// Get or set the sum function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> T</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T2) = T1 + T2</b>
        ///// </remarks>
        //public Func<T, T, T> Sum { get; set; }

        ///// <summary>
        ///// Get or Set the division function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> T</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T2) = T1 / T2</b>
        ///// </remarks>
        //public Func<T, T, T> Division { get; set; }

        ///// <summary>
        ///// Get or Set the subtraction function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> T</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T2) = T1 - T2</b>
        ///// </remarks>
        //public Func<T, T, T> Subtraction { get; set; }

        ///// <summary>
        ///// Get or Set the GreaterThan function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> bool</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T2) = T1 &gt; T2</b>
        ///// </remarks>
        //public Func<T, T, bool> GreaterThan { get; set; }

        ///// <summary>
        ///// Get or Set the SmallerThan function for the elements
        ///// of this NumericMatrix instance.
        ///// </summary>
        ///// <remarks>
        ///// The function must be:
        /////     <b>f: (T, T) -> bool</b>
        ///// and must do the equivalent operation of the following:
        /////     <b>f(T1, T1) = T1 &st; T2</b>
        ///// </remarks>
        //public Func<T, T, bool> SmallerThan { get; set; }

        ///// <summary>
        ///// Get or Set the identity element for the sum
        ///// operation of the elements of this NumericMatrix instance.
        ///// </summary>
        //public T SumIdentity { get; set; }

        ///// <summary>
        ///// Get or Set the identity element for the multiplication
        ///// operation of the elements of this NumericMatrix instance.
        ///// </summary>
        //public T MultiplicationIdentity { get; set; }

        ///// <summary>
        ///// Check if this NumericMatrix is a diagonal one or not.
        ///// </summary>
        //public bool IsDiagonal
        //{
        //    get
        //    {
        //        bool result = IsSquare;

        //        //lock (thisLock)
        //        //{
        //        for (int i = 0; result && i < Rows; i++)
        //        {
        //            for (int j = 0; result && j < Columns; j++)
        //            {
        //                if (i != j)
        //                {
        //                    result = (this[i, j].Equals(SumIdentity == null ? Utilities.SumId<T>() : SumIdentity));
        //                }
        //            }
        //        }
        //        //}

        //        return result;
        //    }
        //}

        /////// <summary>
        /////// Check asynchronously if this NumericMatrix is a diagonal one or not.
        /////// </summary>
        ////public Task<bool> IsDiagonalAsync
        ////{
        ////    get
        ////    {
        ////        return Task.Run(() => IsDiagonal);
        ////    }
        ////}

        ///// <summary>
        ///// Check if this NumericMatrix is singular or not.
        ///// </summary>
        //public bool IsSingular
        //{
        //    get
        //    {
        //        return IsSquare && GetDeterminant().Equals(SumIdentity);
        //    }
        //}

        /////// <summary>
        /////// Check asynchronously if this NumericMatrix is singular or not.
        /////// </summary>
        ////public Task<bool> IsSingularAsync
        ////{
        ////    get
        ////    {
        ////        return Task.Run(() => IsSingular);
        ////    }
        ////}

        ///// <summary>
        ///// Check if this NumericMatrix is weakly diagonally dominant or not.
        ///// </summary>
        //public bool IsWeaklyDiagonallyDominant
        //{
        //    get
        //    {
        //        bool result = IsSquare;
        //        T diagonalElement;
        //        T sumResult;

        //        //lock (thisLock)
        //        //{
        //        var smallerThanFunc = SmallerThan == null
        //                                ? Utilities.SmallerThan<T>()
        //                                : SmallerThan;

        //        for (int i = 0; result && i < Rows; i++)
        //        {
        //            diagonalElement = this[i, i];
        //            sumResult = this[i, 0];

        //            for (int j = 1; result && j < Columns; j++)
        //            {
        //                sumResult = Sum(sumResult, this[i, j]);
        //            }

        //            result = result && smallerThanFunc(sumResult, diagonalElement);
        //        }
        //        //}

        //        return result;
        //    }
        //}

        /////// <summary>
        /////// Check asynchronously if this NumericMatrix is weakly diagonally dominant or not.
        /////// </summary>
        ////public Task<bool> IsWeaklyDiagonallyDominantAsync
        ////{
        ////    get
        ////    {
        ////        return Task.Run(() => IsWeaklyDiagonallyDominant);
        ////    }
        ////}

        ///// <summary>
        ///// Check if this NumericMatrix is strongly diagonally dominant or not.
        ///// </summary>
        //public bool IsStronglyDiagonallyDominant
        //{
        //    get
        //    {
        //        bool result = IsSquare;
        //        T diagonalElement;
        //        T sumResult;

        //        //lock (thisLock)
        //        //{
        //        var smallerThanFunc = SmallerThan != null
        //                                ? SmallerThan
        //                                : Utilities.SmallerThan<T>();

        //        for (int i = 0; result && i < Rows; i++)
        //        {
        //            diagonalElement = this[i, i];
        //            sumResult = this[i, 0];

        //            for (int j = 1; result && j < Columns; j++)
        //            {
        //                sumResult = Sum(sumResult, this[i, j]);
        //            }

        //            result = result && smallerThanFunc(sumResult, diagonalElement);
        //        }
        //        //}

        //        return result;
        //    }
        //}

        /////// <summary>
        /////// Check asynchronously if this NumericMatrix is strongly diagonally dominant or not.
        /////// </summary>
        ////public Task<bool> IsStronglyDiagonallyDominantAsync
        ////{
        ////    get
        ////    {
        ////        return Task.Run(() => IsStronglyDiagonallyDominant);
        ////    }
        ////}

        ///// <summary>
        ///// Check if this NumericMatrix is weakly or strongly diagonally dominant.
        ///// </summary>
        //public bool IsDiagonallyDominant
        //{
        //    get
        //    {
        //        return IsStronglyDiagonallyDominant || IsWeaklyDiagonallyDominant;
        //    }
        //}

        /////// <summary>
        /////// Check asynchronously if this NumericMatrix is weakly or strongly diagonally dominant.
        /////// </summary>
        ////public Task<bool> IsDiagonallyDominantAsync
        ////{
        ////    get
        ////    {
        ////        return Task.Run(() => IsDiagonallyDominant);
        ////    }
        ////}

        //#endregion

        //#region constructors
        //public NumericMatrix() : base() { }

        //public NumericMatrix(int dimension) : base(dimension) { }

        //public NumericMatrix(int rows, int columns) : base(rows, columns) { }
        //#endregion

        //#region methods
        //#region addition methods
        ///// <summary>
        ///// A method that sums this NumericMatrix to another.
        ///// </summary>
        ///// <param name="toAdd">
        ///// The NumericMatrix to add to this NumericMatrix instance.
        ///// </param>
        ///// <returns>
        ///// The resulting NumericMatrix of the sum between the two matrices.
        ///// </returns>
        ///// <remarks>
        ///// This method works for the standard .NET numeric types.
        ///// </remarks>
        ///// <exception cref="InvalidOperationException">
        ///// Thrown if the number of rows and columns is different in 
        ///// this NumericMatrix and the other NumericMatrix. 
        ///// </exception>
        //public NumericMatrix<T> Add(NumericMatrix<T> toAdd)
        //{
        //    return Add(toAdd, Utilities.Sum<T>());
        //}

        ///// <summary>
        ///// A method that sums this NumericMatrix to another.
        ///// </summary>
        ///// <param name="toAdd">
        ///// The NumericMatrix to add to this NumericMatrix instance.
        ///// </param>
        ///// <param name="sumFunction">
        ///// The function used to sum two elements of the matrices.
        ///// </param>
        ///// <returns>
        ///// The resulting NumericMatrix of the sum between the two matrices.
        ///// </returns>
        ///// <exception cref="InvalidOperationException">
        ///// Thrown if the number of rows and columns is different in 
        ///// this NumericMatrix and the other NumericMatrix. 
        ///// </exception>
        //public NumericMatrix<T> Add(NumericMatrix<T> toAdd, Func<T, T, T> sumFunction)
        //{
        //    if (this.Rows != toAdd.Rows)
        //    {
        //        throw new InvalidOperationException("The number of rows between the two matrices is not equal.");
        //    }

        //    if (this.Columns != toAdd.Columns)
        //    {
        //        throw new InvalidOperationException("The number of columns between the two matrices is not equal.");
        //    }

        //    NumericMatrix<T> result = new NumericMatrix<T>(Rows, Columns);

        //    for (int i = 0; i < Rows; i++)
        //    {
        //        for (int j = 0; j < Columns; j++)
        //        {
        //            result[i, j] = sumFunction(this[i, j], toAdd[i, j]);
        //        }
        //    }

        //    return result;
        //}

        /////// <summary>
        /////// A method that sums asynchronously this NumericMatrix to another.
        /////// </summary>
        /////// <param name="toAdd">
        /////// The NumericMatrix to add to this NumericMatrix instance.
        /////// </param>
        /////// <returns>
        /////// The resulting NumericMatrix of the sum between the two matrices.
        /////// </returns>
        /////// <remarks>
        /////// This method works for the standard .NET numeric types.
        /////// </remarks>
        /////// <exception cref="InvalidOperationException">
        /////// Thrown if the number of rows and columns is different in 
        /////// this NumericMatrix and the other NumericMatrix. 
        /////// </exception>
        ////public Task<NumericMatrix<T>> AddAsync(NumericMatrix<T> toAdd)
        ////{
        ////    return Task.Run(() => this.Add(toAdd));
        ////}

        /////// <summary>
        /////// A method that sums asynchrounously this NumericMatrix to another.
        /////// </summary>
        /////// <param name="toAdd">
        /////// The NumericMatrix to add to this NumericMatrix instance.
        /////// </param>
        /////// <param name="sumFunction">
        /////// The function used to sum two elements of the matrices.
        /////// </param>
        /////// <returns>
        /////// The resulting NumericMatrix of the sum between the two matrices.
        /////// </returns>
        /////// <exception cref="InvalidOperationException">
        /////// Thrown if the number of rows and columns is different in 
        /////// this NumericMatrix and the other NumericMatrix. 
        /////// </exception>
        ////public Task<NumericMatrix<T>> AddAsync(NumericMatrix<T> toAdd, Func<T, T, T> sumFunction)
        ////{
        ////    return Task.Run(() => this.Add(toAdd, sumFunction));
        ////}
        //#endregion

        //#region multiplication methods
        ///// <summary>
        ///// A method that multiplies this NumericMatrix to another.
        ///// </summary>
        ///// <param name="toAdd">
        ///// The NumericMatrix to multiply to this NumericMatrix instance.
        ///// </param>
        ///// <returns>
        ///// The resulting NumericMatrix of the multiplication between the two matrices.
        ///// </returns>
        ///// <remarks>
        ///// This method works for the standard .NET numeric types.
        ///// </remarks>
        ///// <exception cref="InvalidOperationException">
        ///// Thrown if the number of rows in the multiplicator NumericMatrix 
        ///// and the number of columns in this NumericMatrix are different.
        ///// </exception>
        //public NumericMatrix<T> Multiply(NumericMatrix<T> multiplicator)
        //{
        //    return Multiply(
        //        multiplicator,
        //        Multiplication == null ? Utilities.Multiplication<T>() : Multiplication,
        //        Sum == null ? Utilities.Sum<T>() : Sum,
        //        SumIdentity == null ? Utilities.SumId<T>() : SumIdentity);
        //}

        ///// <summary>
        ///// A method that multiplies this NumericMatrix to another.
        ///// </summary>
        ///// <param name="toAdd">
        ///// The NumericMatrix to multiply to this NumericMatrix instance.
        ///// </param>
        ///// <param name="multiplicationFunction">
        ///// The function used for the multiplication of the NumericMatrix's elements.
        ///// </param>
        ///// <param name="sumFunction">
        ///// The function used for the sum of the NumericMatrix's elements.
        ///// </param>
        ///// <param name="sumId">
        ///// The function used to get the sum id for the NumericMatrix's elements.
        ///// </param>
        ///// <returns>
        ///// The resulting NumericMatrix of the multiplication between the two matrices.
        ///// </returns>
        ///// <remarks>
        ///// This method works for the standard .NET numeric types.
        ///// </remarks>
        ///// <exception cref="InvalidOperationException">
        ///// Thrown if the number of rows in the multiplicator NumericMatrix 
        ///// and the number of columns in this NumericMatrix are different.
        ///// </exception>
        //public NumericMatrix<T> Multiply(NumericMatrix<T> multiplicator,
        //                        Func<T, T, T> multiplicationFunction,
        //                        Func<T, T, T> sumFunction,
        //                        T sumId)
        //{
        //    if (this.Columns != multiplicator.Rows)
        //    {
        //        throw new InvalidOperationException(string.Format(
        //            "{0} {1}",
        //            "The number of columns in this NumericMatrix is different from the number of rows in the multiplicator NumericMatrix.",
        //            "They must be equal."));
        //    }

        //    NumericMatrix<T> result = new NumericMatrix<T>(this.Rows, multiplicator.Columns);
        //    T sum = sumId;

        //    for (int i = 0; i < this.Rows; i++)
        //    {
        //        for (int j = 0; j < multiplicator.Columns; j++)
        //        {
        //            for (int k = 0; k < this.Rows; k++)
        //            {
        //                sum = sumFunction(sum, multiplicationFunction(this[i, k], multiplicator[k, j]));
        //            }

        //            result[i, j] = sum;
        //        }
        //    }

        //    return result;
        //}

        /////// <summary>
        /////// A method that asynchronously multiplies this NumericMatrix to another.
        /////// </summary>
        /////// <param name="toAdd">
        /////// The NumericMatrix to multiply to this NumericMatrix instance.
        /////// </param>
        /////// <returns>
        /////// The resulting NumericMatrix of the multiplication between the two matrices.
        /////// </returns>
        /////// <remarks>
        /////// This method works for the standard .NET numeric types.
        /////// </remarks>
        /////// <exception cref="InvalidOperationException">
        /////// Thrown if the number of rows in the multiplicator NumericMatrix 
        /////// and the number of columns in this NumericMatrix are different.
        /////// </exception>
        ////public Task<NumericMatrix<T>> MultiplyAsync(NumericMatrix<T> multiplicator)
        ////{
        ////    return Task.Run(() => Multiply(multiplicator));
        ////}

        /////// <summary>
        /////// A method that multiplies this NumericMatrix to another.
        /////// </summary>
        /////// <param name="toAdd">
        /////// The NumericMatrix to multiply to this NumericMatrix instance.
        /////// </param>
        /////// <param name="multiplicationFunction">
        /////// The function used for the multiplication of the NumericMatrix's elements.
        /////// </param>
        /////// <param name="sumFunction">
        /////// The function used for the sum of the NumericMatrix's elements.
        /////// </param>
        /////// <param name="getSumId">
        /////// The function used to get the sum id for the NumericMatrix's elements.
        /////// </param>
        /////// <returns>
        /////// The resulting NumericMatrix of the multiplication between the two matrices.
        /////// </returns>
        /////// <remarks>
        /////// This method works for the standard .NET numeric types.
        /////// </remarks>
        /////// <exception cref="InvalidOperationException">
        /////// Thrown if the number of rows in the multiplicator NumericMatrix 
        /////// and the number of columns in this NumericMatrix are different.
        /////// </exception>
        ////public Task<NumericMatrix<T>> MultiplyAsync(NumericMatrix<T> multiplicator,
        ////                                    Func<T, T, T> multiplicationFunction,
        ////                                    Func<T, T, T> sumFunction,
        ////                                    T sumId)
        ////{
        ////    return Task.Run(() => Multiply(multiplicator, multiplicationFunction, sumFunction, sumId));
        ////}

        ///// <summary>
        ///// A method that multiplies this NumericMatrix to a scalar.
        ///// </summary>
        ///// <param name="multiplicator">
        ///// The scalar with which this NumericMatrix must be multiplied.
        ///// </param>
        ///// <returns>
        ///// The NumericMatrix resulting from the multiplication operation.
        ///// </returns>
        ///// <remarks>
        ///// This method works for the .NET's standard numeric types.
        ///// </remarks>
        //public NumericMatrix<T> Multiply(T multiplicator)
        //{
        //    return Multiply(
        //        multiplicator,
        //        Multiplication == null ? Utilities.Multiplication<T>() : Multiplication);
        //}

        ///// <summary>
        ///// A method that multiplies this NumericMatrix to a scalar.
        ///// </summary>
        ///// <param name="multiplicator">
        ///// The scalar with which this NumericMatrix must be multiplied.
        ///// </param>
        ///// <param name="multiplicationFunction">
        ///// The function used for the multiplication of the elements.
        ///// </param>
        ///// <returns>
        ///// The NumericMatrix resulting from the multiplication operation.
        ///// </returns>
        //public NumericMatrix<T> Multiply(T multiplicator, Func<T, T, T> multiplicationFunction)
        //{
        //    NumericMatrix<T> result = new NumericMatrix<T>(Rows, Columns);

        //    for (int i = 0; i < Rows; i++)
        //    {
        //        for (int j = 0; j < Columns; j++)
        //        {
        //            result[i, j] = multiplicationFunction(this[i, j], multiplicator);
        //        }
        //    }

        //    return result;
        //}

        /////// <summary>
        /////// A method that asynchronously multiplies this NumericMatrix to a scalar.
        /////// </summary>
        /////// <param name="multiplicator">
        /////// The scalar with which this NumericMatrix must be multiplied.
        /////// </param>
        /////// <returns>
        /////// The NumericMatrix resulting from the multiplication operation.
        /////// </returns>
        /////// <remarks>
        /////// This method works for the .NET's standard numeric types.
        /////// </remarks>
        ////public Task<NumericMatrix<T>> MultiplyAsync(T multiplicator)
        ////{
        ////    return Task.Run(() => Multiply(multiplicator));
        ////}

        /////// <summary>
        /////// A method that asynchronously multiplies this NumericMatrix to a scalar.
        /////// </summary>
        /////// <param name="multiplicator">
        /////// The scalar with which this NumericMatrix must be multiplied.
        /////// </param>
        /////// <param name="multiplicationFunction">
        /////// The function used for the multiplication of the elements.
        /////// </param>
        /////// <returns>
        /////// The NumericMatrix resulting from the multiplication operation.
        /////// </returns>
        ////public Task<NumericMatrix<T>> MultiplyAsync(T multiplicator, Func<T, T, T> multiplicationFunction)
        ////{
        ////    return Task.Run(() => Multiply(multiplicator, multiplicationFunction));
        ////}

        /////// <summary>
        /////// A method that multiplies a NumericMatrix's row with another NumericMatrix's column.
        /////// </summary>
        /////// <param name="row">
        /////// The row to multiply to the specified column.
        /////// </param>
        /////// <param name="column">
        /////// The column to multiply to the specified row.
        /////// </param>
        /////// <returns>
        /////// The result of the multiplication.
        /////// </returns>
        ////private static T Multiply(T[] row, T[] column, Func<T, T, T> multiply, Func<T, T, T> add, T sumId)
        ////{
        ////    T result = sumId;

        ////    for (int i = 0; i < row.Length; i++)
        ////    {
        ////        result = add(result, multiply(row[i], column[i]));
        ////    }

        ////    return result;
        ////}
        //#endregion

        //#region get row

        /////// <summary>
        /////// A method used to get the row in the specified index without locking it.
        /////// </summary>
        /////// <param name="index">
        /////// The 0-based index of the row.
        /////// </param>
        /////// <returns>
        /////// The row in the specified index.
        /////// </returns>
        /////// <remarks>
        /////// Possible subject of change.
        /////// </remarks>
        ////private T[] GetRowNoLock(int index)
        ////{
        ////    T[] result = new T[Columns];

        ////    for (int i = 0; i < Columns; i++)
        ////    {
        ////        result[i] = base[index, i];
        ////    }

        ////    return result;
        ////}
        //#endregion

        //#region get column
        /////// <summary>
        /////// A method used to get the column in the specified index without locking it.
        /////// </summary>
        /////// <param name="index">
        /////// The 0-based index of the column.
        /////// </param>
        /////// <returns>
        /////// The column in the specified index.
        /////// </returns>
        /////// <remarks>
        /////// Possible subject of change.
        /////// </remarks>
        ////private T[] GetColumnNoLock(int index)
        ////{
        ////    T[] result = new T[Rows];

        ////    for (int i = 0; i < Rows; i++)
        ////    {
        ////        result[i] = base[i, index];
        ////    }

        ////    return result;
        ////}
        //#endregion

        //#region get trace
        ///// <summary>
        ///// A method that calculates the trace of this NumericMatrix.
        ///// </summary>
        ///// <returns>
        ///// The trace of this square NumericMatrix.
        ///// </returns>
        ///// <exception cref="InvalidOperationException">
        ///// If this NumericMatrix is not square.
        ///// </exception>
        ///// <remarks>
        ///// This method works for .NET's standard numeric types.
        ///// </remarks>
        //public T GetTrace()
        //{
        //    return GetTrace(Utilities.Sum<T>(), Utilities.SumId<T>());
        //}

        ///// <summary>
        ///// A method that calculates the trace of this NumericMatrix.
        ///// </summary>
        ///// <param name="elementSumFunction">
        ///// The function used to sum the elements of the NumericMatrix.
        ///// </param>
        ///// <param name="sumId">
        ///// The function used to get the id element of the sum 
        ///// operation.
        ///// </param>
        ///// <returns>
        ///// The trace of this square NumericMatrix.
        ///// </returns>
        ///// <exception cref="InvalidOperationException">
        ///// If this NumericMatrix is not square.
        ///// </exception>
        //public T GetTrace(Func<T, T, T> elementSumFunction,
        //                    T sumId)
        //{
        //    if (!IsSquare)
        //    {
        //        throw new InvalidOperationException("This operation is possible only on square matrices.");
        //    }

        //    T result = sumId;

        //    for (int i = 0; i < Rows; i++)
        //    {
        //        result = elementSumFunction(result, this[i, i]);
        //    }

        //    return result;
        //}

        /////// <summary>
        /////// A method that asynchronously calculates the trace of this NumericMatrix.
        /////// </summary>
        /////// <returns>
        /////// The trace of this square NumericMatrix.
        /////// </returns>
        /////// <exception cref="InvalidOperationException">
        /////// If this NumericMatrix is not square.
        /////// </exception>
        /////// <remarks>
        /////// This method works for .NET's standard numeric types.
        /////// </remarks>
        ////public Task<T> GetTraceAsync()
        ////{
        ////    return Task.Run(() => GetTrace());
        ////}

        /////// <summary>
        /////// A method that asynchronously calculates the trace of 
        /////// this NumericMatrix.
        /////// </summary>
        /////// <param name="elementSumFunction">
        /////// The function used to sum the elements of the NumericMatrix.
        /////// </param>
        /////// <param name="sumId">
        /////// The function used to get the id element of the sum 
        /////// operation.
        /////// </param>
        /////// <returns>
        /////// The trace of this square NumericMatrix.
        /////// </returns>
        /////// <exception cref="InvalidOperationException">
        /////// If this NumericMatrix is not square.
        /////// </exception>
        ////public Task<T> GetTraceAsync(Func<T, T, T> elementSumFunction,
        ////                            T sumId)
        ////{
        ////    return Task.Run(() => GetTrace(elementSumFunction, sumId));
        ////}
        //#endregion

        //#region get determinant
        ///// <summary>
        ///// A method that calculates and returns the 
        ///// determinant of this square NumericMatrix.
        ///// </summary>
        ///// <returns>
        ///// The determinant of this square NumericMatrix.
        ///// </returns>
        ///// <remarks>
        ///// This method works for .NET's standard numeric 
        ///// types.
        ///// </remarks>
        //public T GetDeterminant()
        //{
        //    return GetDeterminant(
        //        elementMultiplicationFunction: Multiplication == null ? Utilities.Multiplication<T>() : Multiplication,
        //        elementSumFunction: Sum == null ? Utilities.Sum<T>() : Sum,
        //        elementSubtractionFunction: Subtraction == null ? Utilities.Subtraction<T>() : Subtraction,
        //        multiplicationId: MultiplicationIdentity == null ? Utilities.MultiplicationId<T>() : MultiplicationIdentity,
        //        sumId: SumIdentity == null ? Utilities.SumId<T>() : SumIdentity);
        //}

        ///// <summary>
        ///// A method that calculates and returns the 
        ///// determinant of this square NumericMatrix.
        ///// </summary>
        ///// <param name="elementMultiplicationFunction">
        ///// The function used for the multiplication of the
        ///// elements of this NumericMatrix.
        ///// </param>
        ///// <param name="elementSumFunction">
        ///// The function used for the sum of the elements of
        ///// this NumericMatrix.
        ///// </param>
        ///// <param name="elementSubtractionFunction">
        ///// The function used for the subtraction of the 
        ///// elements of this NumericMatrix.
        ///// </param>
        ///// <param name="multiplicationId">
        ///// The function used to get the multiplication id 
        ///// for the elements of this NumericMatrix.
        ///// </param>
        ///// <param name="sumId">
        ///// The function used to get the sum id for the 
        ///// elements of this NumericMatrix.
        ///// </param>
        ///// <returns>
        ///// The determinant of this square NumericMatrix.
        ///// </returns>
        //public T GetDeterminant(Func<T, T, T> elementMultiplicationFunction,
        //                        Func<T, T, T> elementSumFunction,
        //                        Func<T, T, T> elementSubtractionFunction,
        //                        T multiplicationId,
        //                        T sumId)
        //{
        //    if (!IsSquare)
        //    {
        //        throw new InvalidOperationException("This operation is possible only on square matrices.");
        //    }

        //    T topDownResult,
        //        bottomUpResult,
        //        tempResult;

        //    //lock (thisLock)
        //    //{
        //    topDownResult = sumId;
        //    bottomUpResult = sumId;
        //    tempResult = multiplicationId;

        //    // let's calculate the sum of the top-down diagonals
        //    for (int i = 0; i < Rows; i++)
        //    {
        //        tempResult = multiplicationId;

        //        for (int j = 0; j < Rows; j++)
        //        {
        //            tempResult = elementMultiplicationFunction(tempResult, this[j, (i + j) % Rows]);
        //        }

        //        topDownResult = elementSumFunction(topDownResult, tempResult);
        //    }

        //    // now let's calculate the sum of the bottom-up diagonals
        //    for (int i = 0; i < Rows; i++)
        //    {
        //        tempResult = multiplicationId;

        //        for (int j = 0; j < Rows; j++)
        //        {
        //            tempResult = elementMultiplicationFunction(tempResult, this[(Rows - j - 1), (i + j) % Rows]);
        //        }

        //        bottomUpResult = elementSumFunction(bottomUpResult, tempResult);
        //    }
        //    //}

        //    return elementSumFunction(topDownResult, bottomUpResult);
        //}

        /////// <summary>
        /////// A method that asynchronously calculates and returns the 
        /////// determinant of this square NumericMatrix.
        /////// </summary>
        /////// <returns>
        /////// The determinant of this square NumericMatrix.
        /////// </returns>
        /////// <remarks>
        /////// This method works for .NET's standard numeric 
        /////// types.
        /////// </remarks>
        ////public Task<T> GetDeterminantAsync()
        ////{
        ////    return Task.Run(() => GetDeterminant());
        ////}

        /////// <summary>
        /////// A method that asynchronously calculates and returns the 
        /////// determinant of this square NumericMatrix.
        /////// </summary>
        /////// <param name="elementMultiplicationFunction">
        /////// The function used for the multiplication of the
        /////// elements of this NumericMatrix.
        /////// </param>
        /////// <param name="elementSumFunction">
        /////// The function used for the sum of the elements of
        /////// this NumericMatrix.
        /////// </param>
        /////// <param name="elementSubtractionFunction">
        /////// The function used for the subtraction of the 
        /////// elements of this NumericMatrix.
        /////// </param>
        /////// <param name="multiplicationId">
        /////// The function used to get the multiplication id 
        /////// for the elements of this NumericMatrix.
        /////// </param>
        /////// <param name="sumId">
        /////// The function used to get the sum id for the 
        /////// elements of this NumericMatrix.
        /////// </param>
        /////// <returns>
        /////// The determinant of this square NumericMatrix.
        /////// </returns>
        ////public Task<T> GetDeterminantAsync(Func<T, T, T> elementMultiplicationFunction,
        ////                        Func<T, T, T> elementSumFunction,
        ////                        Func<T, T, T> elementSubtractionFunction,
        ////                        T multiplicationId,
        ////                        T sumId)
        ////{
        ////    return Task.Run(() => GetDeterminant(
        ////        elementMultiplicationFunction,
        ////        elementSumFunction,
        ////        elementSubtractionFunction,
        ////        multiplicationId,
        ////        sumId));
        ////}
        //#endregion


        //#region get identity
        ///// <summary>
        ///// A method that gets the Identity Matrix 
        ///// of a given size for the multiplication
        ///// operation.
        ///// </summary>
        ///// <param name="size">
        ///// The desired size of the Identity Matrix.
        ///// </param>
        ///// <returns>
        ///// The Identity Matrix of a given size.
        ///// </returns>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified size is smaller than 1.
        ///// </exception>
        ///// <remarks>
        ///// This method works for .NET's standard 
        ///// numeric types.
        ///// </remarks>
        //public static Matrix<T> GetMultiplicationIdentity(int size = 2)
        //{
        //    return GetMultiplicationIdentity(
        //        size,
        //        Utilities.MultiplicationId<T>(),
        //        Utilities.SumId<T>());
        //}

        ///// <summary>
        ///// A method that gets the Identity Matrix 
        ///// of a given size.
        ///// </summary>
        ///// <param name="size">
        ///// The desired size of the Identity Matrix.
        ///// </param>
        ///// <param name="elementMultiplicationId">
        ///// The function that returns the element's id for the 
        ///// multiplication operation.
        ///// </param>
        ///// <param name="elementSumId">
        ///// The function that returns the element's id for the 
        ///// addition operation.
        ///// </param>
        ///// <returns>
        ///// The Identity Matrix of a given size.
        ///// </returns>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified size is smaller than 1.
        ///// </exception>
        //public static Matrix<T> GetMultiplicationIdentity(
        //                int size,
        //                T elementMultiplicationId,
        //                T elementSumId)
        //{
        //    if (size < 1)
        //    {
        //        throw new ArgumentOutOfRangeException("size", "The size cannot be smaller than 1.");
        //    }

        //    Matrix<T> result = new Matrix<T>(size);
        //    T multiplicationId = elementMultiplicationId;
        //    T sumId = elementSumId;

        //    for (int i = 0; i < size; i++)
        //    {
        //        for (int j = 0; j < size; j++)
        //        {
        //            result[i, j] = (i == j) ? multiplicationId : sumId;
        //        }
        //    }

        //    return result;
        //}

        ///// <summary>
        ///// A method that gets the square Identity Matrix 
        ///// of a given size for the addition operation.
        ///// </summary>
        ///// <param name="size">
        ///// The desired size of the square Identity Matrix.
        ///// </param>
        ///// <returns>
        ///// The Identity Matrix of a given size.
        ///// </returns>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified sizes are smaller than 1.
        ///// </exception>
        ///// <remarks>
        ///// This method works for .NET's standard numeric 
        ///// types.
        ///// </remarks>
        //public static Matrix<T> GetSumIdentity(int size = 2)
        //{
        //    return GetSumIdentity(size, size);
        //}

        ///// <summary>
        ///// A method that gets the Identity Matrix 
        ///// of a given size for the addition operation.
        ///// </summary>
        ///// <param name="rows">
        ///// The desired rowsize of the Identity Matrix.
        ///// </param>
        ///// <param name="columns">
        ///// The desired column size of the Identity Matrix.
        ///// </param>
        ///// <returns>
        ///// The Identity Matrix of a given size.
        ///// </returns>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified sizes are smaller than 1.
        ///// </exception>
        ///// <remarks>
        ///// This method works for .NET's standard numeric 
        ///// types.
        ///// </remarks>
        //public static Matrix<T> GetSumIdentity(int rows, int columns)
        //{
        //    return GetSumIdentity(rows, columns, Utilities.SumId<T>());
        //}

        ///// <summary>
        ///// A method that gets the Identity Matrix 
        ///// of a given size for the addition operation.
        ///// </summary>
        ///// <param name="rows">
        ///// The desired rowsize of the Identity Matrix.
        ///// </param>
        ///// <param name="columns">
        ///// The desired column size of the Identity Matrix.
        ///// </param>
        ///// <param name="elementSumId">
        ///// The function that gets the id of the Matrix's 
        ///// element's for the addition operation.
        ///// </param>
        ///// <returns>
        ///// The Identity Matrix of a given size.
        ///// </returns>
        ///// <exception cref="ArgumentOutOfRangeException">
        ///// If the specified sizes are smaller than 1.
        ///// </exception>
        ///// <remarks>
        ///// This method works for .NET's standard numeric 
        ///// types.
        ///// </remarks>
        //public static Matrix<T> GetSumIdentity(
        //                            int rows,
        //                            int columns,
        //                            T elementSumId)
        //{
        //    if (rows < 1)
        //    {
        //        throw new ArgumentOutOfRangeException("rows", "The number of rows must be greater than 0.");
        //    }

        //    if (columns < 1)
        //    {
        //        throw new ArgumentOutOfRangeException("columns", "The number of columns must be greater than 0.");
        //    }

        //    Matrix<T> result = new Matrix<T>(rows, columns);
        //    T sumId = elementSumId;

        //    for (int i = 0; i < rows; i++)
        //    {
        //        for (int j = 0; j < columns; j++)
        //        {
        //            result[i, j] = sumId;
        //        }
        //    }

        //    return result;
        //}

        /////// <summary>
        /////// A method that asynchronously gets the Identity Matrix 
        /////// of a given size for the multiplication operation.
        /////// </summary>
        /////// <param name="size">
        /////// The desired size of the Identity Matrix.
        /////// </param>
        /////// <returns>
        /////// The Identity Matrix of a given size.
        /////// </returns>
        /////// <exception cref="ArgumentOutOfRangeException">
        /////// If the specified size is smaller than 1.
        /////// </exception>
        /////// <remarks>
        /////// This method works for .NET's standard 
        /////// numeric types.
        /////// </remarks>
        ////public static Task<Matrix<T>> GetMultiplicationIdentityAsync(int size = 2)
        ////{
        ////    return Task.Run(() => GetMultiplicationIdentity(size));
        ////}

        /////// <summary>
        /////// A method that asynchronously gets the Identity Matrix 
        /////// of a given size.
        /////// </summary>
        /////// <param name="size">
        /////// The desired size of the Identity Matrix.
        /////// </param>
        /////// <param name="elementMultiplicationId">
        /////// The function that returns the element's id for the 
        /////// multiplication operation.
        /////// </param>
        /////// <param name="elementSumId">
        /////// The function that returns the element's id for the 
        /////// addition operation.
        /////// </param>
        /////// <returns>
        /////// The Identity Matrix of a given size.
        /////// </returns>
        /////// <exception cref="ArgumentOutOfRangeException">
        /////// If the specified size is smaller than 1.
        /////// </exception>
        ////public static Task<Matrix<T>> GetMultiplicationIdentityAsync(
        ////            int size,
        ////            T elementMultiplicationId,
        ////            T elementSumId)
        ////{
        ////    return Task.Run(() => GetMultiplicationIdentity(size, elementMultiplicationId, elementSumId));
        ////}

        /////// <summary>
        /////// A method that asynchronously gets the Identity Matrix 
        /////// of a given size for the addition operation.
        /////// </summary>
        /////// <param name="rows">
        /////// The desired rowsize of the Identity Matrix.
        /////// </param>
        /////// <param name="columns">
        /////// The desired column size of the Identity Matrix.
        /////// </param>
        /////// <returns>
        /////// The Identity Matrix of a given size.
        /////// </returns>
        /////// <exception cref="ArgumentOutOfRangeException">
        /////// If the specified sizes are smaller than 1.
        /////// </exception>
        /////// <remarks>
        /////// This method works for .NET's standard numeric 
        /////// types.
        /////// </remarks>
        ////public static Task<Matrix<T>> GetSumIdentityAsync(int rows, int columns)
        ////{
        ////    return Task.Run(() => GetSumIdentity(rows, columns));
        ////}

        /////// <summary>
        /////// A method that asynchronously gets the Identity Matrix 
        /////// of a given size for the addition operation.
        /////// </summary>
        /////// <param name="rows">
        /////// The desired rowsize of the Identity Matrix.
        /////// </param>
        /////// <param name="columns">
        /////// The desired column size of the Identity Matrix.
        /////// </param>
        /////// <param name="elementSumId">
        /////// The function that gets the id of the Matrix's 
        /////// element's for the addition operation.
        /////// </param>
        /////// <returns>
        /////// The Identity Matrix of a given size.
        /////// </returns>
        /////// <exception cref="ArgumentOutOfRangeException">
        /////// If the specified sizes are smaller than 1.
        /////// </exception>
        /////// <remarks>
        /////// This method works for .NET's standard numeric 
        /////// types.
        /////// </remarks>
        ////public static Task<Matrix<T>> GetSumIdentityAsync(
        ////                            int rows,
        ////                            int columns,
        ////                            T elementSumId)
        ////{
        ////    return Task.Run(() => GetSumIdentity(rows, columns, elementSumId));
        ////}
        //#endregion
        //#endregion

        //#region operators
        ///// <summary>
        ///// Get the Matrix result of the sum between two other matrices.
        ///// </summary>
        ///// <param name="left">
        ///// The Matrix on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The Matrix on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// The Matrix result of the sum between the two specified matrices.
        ///// </returns>
        ///// <remarks>
        ///// This operator works correctly for the .NET's standard
        ///// numeric types.
        ///// </remarks>
        //public static NumericMatrix<T> operator +(NumericMatrix<T> left, NumericMatrix<T> right)
        //{
        //    return left.Add(right);
        //}

        ///// <summary>
        ///// Get the Matrix result of the multiplication between 
        ///// two other matrices.
        ///// </summary>
        ///// <param name="left">
        ///// The Matrix on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The Matrix on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// The Matrix result of the multiplication between the 
        ///// two specified matrices.
        ///// </returns>
        ///// <remarks>
        ///// This operator works correctly for the .NET's standard
        ///// numeric types.
        ///// </remarks>
        //public static NumericMatrix<T> operator *(NumericMatrix<T> left, NumericMatrix<T> right)
        //{
        //    return left.Multiply(right);
        //}

        ///// <summary>
        ///// Get the Matrix result of the multiplication between 
        ///// a Matrix and a scalar value.
        ///// </summary>
        ///// <param name="left">
        ///// The Matrix on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The scalar value on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// The Matrix result of the multiplication between the 
        ///// Matrix and the scalar.
        ///// </returns>
        ///// <remarks>
        ///// This operator works correctly for the .NET's standard
        ///// numeric types.
        ///// </remarks>
        //public static NumericMatrix<T> operator *(NumericMatrix<T> left, T right)
        //{
        //    return left.Multiply(right);
        //}

        ///// <summary>
        ///// Get the Matrix result of the multiplication between 
        ///// a scalar value and a Matrix.
        ///// </summary>
        ///// <param name="left">
        ///// The scalar value on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The Matrix on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// The Matrix result of the multiplication between the 
        ///// Matrix and the scalar.
        ///// </returns>
        ///// <remarks>
        ///// This operator works correctly for the .NET's standard
        ///// numeric types.
        ///// </remarks>
        //public static NumericMatrix<T> operator *(T left, NumericMatrix<T> right)
        //{
        //    return right.Multiply(left);
        //}

        ///// <summary>
        ///// Check if two matrices are equal.
        ///// </summary>
        ///// <param name="left">
        ///// The scalar value on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The Matrix on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// TRUE if the two matrices are equal, FALSE otherwise.
        ///// </returns>
        //public static bool operator ==(NumericMatrix<T> left, NumericMatrix<T> right)
        //{
        //    return left.Equals(right);
        //}

        ///// <summary>
        ///// Check if two matrices are different.
        ///// </summary>
        ///// <param name="left">
        ///// The scalar value on the left side of the operator.
        ///// </param>
        ///// <param name="right">
        ///// The Matrix on the right side of the operator.
        ///// </param>
        ///// <returns>
        ///// TRUE if the two matrices are different, FALSE otherwise.
        ///// </returns>
        //public static bool operator !=(NumericMatrix<T> left, NumericMatrix<T> right)
        //{
        //    return !(left.Equals(right));
        //}
        //#endregion
    }
}
