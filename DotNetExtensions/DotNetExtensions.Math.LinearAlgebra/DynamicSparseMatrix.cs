﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    public class DynamicSparseMatrix<T> : SparseMatrix<T>
    {
        #region properties
        public override Dictionary<Tuple<int, int>, T> SparseMatrixElements
        {
            get { throw new NotImplementedException(); }
        }

        public override int Columns
        {
            get { throw new NotImplementedException(); }
        }

        public override int Rows
        {
            get { throw new NotImplementedException(); }
        }

        public override T this[int row, int column]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region constructors

        #endregion

        #region methods
        public override SparseMatrix<T> CreateNewSparseMatrix<T>(int rows, int columns)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(Matrix<T> other)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
