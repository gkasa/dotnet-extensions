﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetExtensions.Math.LinearAlgebra
{
    public class DynamicMatrix<T> : Matrix<T>
    {
        #region variables
        private List<List<T>> items;
        private int columns;
        private int rows;
        #endregion

        #region properties
        public override int Columns
        {
            get { return columns; }
        }

        public override int Rows
        {
            get { return rows; }
        }

        public override T this[int row, int column]
        {
            get
            {
                ResizeIfNecessary(row, column);

                return items[row][column];
            }
            set
            {
                ResizeIfNecessary(row, column);

                items[row][column] = value;
            }
        }
        #endregion

        #region constructors
        public DynamicMatrix(int rows, int columns)
        {
            if (rows < 0)
            {
                throw new ArgumentOutOfRangeException("rows", "The number of rows must be greater than, or equal to 0.");
            }

            if (columns < 0)
            {
                throw new ArgumentOutOfRangeException("columns", "The number of columns must be greater than, or equal to 0.");
            }

            this.rows = rows;
            this.columns = columns;
            items = new List<List<T>>(Rows);

            for (int i = 0; i < rows; i++)
            {
                items[i] = new List<T>(columns);
            }
        }

        public DynamicMatrix(int dimension) : this(dimension, dimension) { }

        public DynamicMatrix() : this(2) { }
        #endregion

        #region methods
        public override Matrix<T> CreateNewInstance(int rows, int columns)
        {
            return new DynamicMatrix<T>(rows, columns);
        }

        public override bool Equals(Matrix<T> other)
        {
            return (other is DynamicMatrix<T>) && this.Equals((DynamicMatrix<T>)other);
        }

        public bool Equals(DynamicMatrix<T> other)
        {
            bool result = (Rows == other.Rows) && (Columns == other.Columns);

            for (int i = 0; result && i < Rows; i++)
            {
                for (int j = 0; result && j < Columns; j++)
                {
                    result = this[i, j].Equals(other[i, j]);
                }
            }

            return result;
        }

        private void ResizeIfNecessary(int row, int column)
        {
            if (row >= Rows || column >= Columns)
            {
                // cycle is executed only if the indicated row index is 
                // greater than, or equal to the matrix's current number or rows
                for (int i = Rows; i <= row; i++)
                {
                    items[i] = new List<T>(System.Math.Max(column + 1, Columns));
                }

                if (column >= Columns)
                {
                    int difference = column - Columns + 1;

                    for (int i = 0; i < Rows; i++)
                    {
                        for (int j = 0; j < difference; j++)
                        {
                            items[i].Add(default(T));
                        }
                    }
                }

                rows = System.Math.Max(row + 1, Rows);
                columns = System.Math.Max(column + 1, Columns);
            }
        }
        #endregion
    }
}
