﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNetExtensions.Math.LinearAlgebra;

namespace DotNetExtensionsUnitTests
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void TestIsSquare()
        {
            Matrix<bool> m1 = new StaticMatrix<bool>(100);
            Matrix<bool> m2 = new StaticMatrix<bool>(100, 200);

            bool m1Expected = true;
            bool m2Expected = false;
            bool m1Actual = m1.IsSquare;
            bool m2Actual = m2.IsSquare;

            Assert.AreEqual(m1Expected, m1Actual, "The IsSquare computation is wrong for actual square matrices.");
            Assert.AreEqual(m2Expected, m2Actual, "The IsSquare computation is wrong for non square matrices.");
        }

        [TestMethod]
        public void TestIsSymmetric()
        {
            Matrix<bool> m1 = new StaticMatrix<bool>(100);
            Matrix<bool> m2 = new StaticMatrix<bool>(100, 101);
            Matrix<bool> m3 = new StaticMatrix<bool>(100);

            for (int i = 0; i < 100; i++)
            {
                m1[i, i] = true;
                m3[i, i] = true;
            }

            m3[0, 1] = true;

            bool m1Expected = true;
            bool m2Expected = false;
            bool m3Expected = false;
            bool m1Actual = m1.IsSymmetric;
            bool m2Actual = m2.IsSymmetric;
            bool m3Actual = m3.IsSymmetric;

            Assert.AreEqual<bool>(m1Expected, m1Actual, "The symmetry test for actual symmetric matrices is wrong.");
            Assert.AreEqual<bool>(m2Expected, m2Actual, "The symmetry test for non-square matrices is wrong.");
            Assert.AreEqual<bool>(m3Expected, m3Actual, "The symmetry test for non symmetric matrices is wrong.");
        }

        [TestMethod]
        public void TestEqual()
        {
            Matrix<bool> m1 = new StaticMatrix<bool>(100);
            Matrix<bool> m2 = new StaticMatrix<bool>(100);
            Matrix<bool> m3 = new StaticMatrix<bool>(100);
            Matrix<bool> m4 = new StaticMatrix<bool>(100, 101);
            Matrix<bool> m5 = new StaticMatrix<bool>(100, 101);
            Matrix<bool> m6 = new StaticMatrix<bool>(100, 101);

            for (int i = 0; i < 100; i++)
            {
                m1[i, i] = true;
                m2[i, i] = true;
                m3[i, i] = true;
                m4[i, i] = true;
                m5[i, i] = true;
                m6[i, i] = true;
            }

            m3[0, 3] = true;
            m5[1, 4] = true;

            bool m1m2Expected = true;
            bool m1m3Expected = false;
            bool m1m4Expected = false;
            bool m4m5Expected = false;
            bool m4m6Expected = true;
            bool m1m2Actual = m1.Equals(m2);
            bool m1m3Actual = m1.Equals(m3);
            bool m1m4Actual = m1.Equals(m4);
            bool m4m5Actual = m4.Equals(m5);
            bool m4m6Actual = m4.Equals(m6);

            Assert.AreEqual<bool>(m1m2Expected, m1m2Actual, "The equal method does not compare correctly equal square matrices.");
            Assert.AreEqual<bool>(m1m3Expected, m1m3Actual, "The equal method does not compare correctly non-equal square matrices.");
            Assert.AreEqual<bool>(m1m4Expected, m1m4Actual, "The equal method does not compare correctly square matrices against non-square ones.");
            Assert.AreEqual<bool>(m4m5Expected, m4m5Actual, "The equal method does not compare correctly non-equal non-square matrices.");
            Assert.AreEqual<bool>(m4m6Expected, m4m6Actual, "The equal method does not compare correctly equal non-square matrices.");

        }

        [TestMethod]
        public void TestTransposeDestinationStructure()
        {
            Matrix<bool> m1 = new StaticMatrix<bool>(100);
            Matrix<bool> m2 = new StaticMatrix<bool>(100, 200);
            Matrix<bool> m1t = m1.Transpose();
            Matrix<bool> m2t = m2.Transpose();

            int m1tExpectedRows = 100;
            int m1tExpectedCols = 100;
            int m2tExpectedRows = 200;
            int m2tExpectedCols = 100;
            int m1tActualRows = m1t.Rows;
            int m1tActualCols = m1t.Columns;
            int m2tActualRows = m2t.Rows;
            int m2tActualCols = m2t.Columns;

            Assert.AreEqual<int>(m1tExpectedCols, m1tActualCols, "The number of columns in the transposed square matrix is different from the expected.");
            Assert.AreEqual<int>(m1tExpectedRows, m1tActualRows, "The number of rows in the transposed square matrix is different from the expected.");
            Assert.AreEqual<int>(m2tExpectedCols, m2tActualCols, "The number of columns in the transposed non-square matrix is different from the expected.");
            Assert.AreEqual<int>(m2tExpectedRows, m2tActualRows, "The number of rows in the transposed non-square matrix is different from the expected.");
        }

        [TestMethod]
        public void TestTransposeDestinationElements()
        {
            Matrix<bool> m1 = new StaticMatrix<bool>(100);
            Matrix<bool> m2 = new StaticMatrix<bool>(100, 200);
            Matrix<bool> m1tExpected = new StaticMatrix<bool>(100);
            Matrix<bool> m2tExpected = new StaticMatrix<bool>(200, 100);

            for (int i = 0; i < 100; i++)
            {
                m1[i, i] = true;
                m1tExpected[i, i] = true;
                m2[i, i] = true;
                m2tExpected[i, i] = true;
            }

            m1[0, 99] = true;
            m1tExpected[99, 0] = true;
            m2[0, 99] = true;
            m2tExpected[99, 0] = true;

            Matrix<bool> m1tActual = m1.Transpose();
            Matrix<bool> m2tActual = m2.Transpose();

            Assert.AreEqual<Matrix<bool>>(m1tExpected, m1tActual, "The values for the square matrices do not match.");
            Assert.AreEqual<Matrix<bool>>(m2tExpected, m2tActual, "The values for the non-square matrices do not match.");
        }
    }
}
